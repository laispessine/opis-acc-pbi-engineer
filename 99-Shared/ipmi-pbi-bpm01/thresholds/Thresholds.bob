<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Thresholds</name>
  <width>820</width>
  <height>820</height>
  <widget type="rectangle" version="2.0.0">
    <name>title-bar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>820</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>title</name>
    <class>TITLE</class>
    <text>IPMI Tool</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>570</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>subtitle</name>
    <class>SUBTITLE</class>
    <text>$(MTCA_PREF)</text>
    <x>550</x>
    <y use_class="true">20</y>
    <width>250</width>
    <height use_class="true">30</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">2</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Main_group</name>
    <x>20</x>
    <y>70</y>
    <width>780</width>
    <height>500</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Main_rectangle</name>
      <width>780</width>
      <height>500</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
      <tooltip>Threshold panel -&gt; Set new thresholds values and then press commit to load them to module.</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Main_label</name>
      <class>HEADER2</class>
      <text>Thresholds</text>
      <width>780</width>
      <height>40</height>
      <font use_class="true">
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Picture_1</name>
      <file>../images/Graph_upper.gif</file>
      <x>490</x>
      <y>50</y>
      <width>290</width>
      <height>200</height>
      <tooltip>Image of upper thresholds levels</tooltip>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Picture_2</name>
      <file>../images/Graph_lower.gif</file>
      <x>490</x>
      <y>270</y>
      <width>290</width>
      <height>200</height>
      <tooltip>Image of lower thresholds levels</tooltip>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>THRLONONCRITVAL_Spinner</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoNonCrt-SP</pv_name>
      <x>380</x>
      <y>300</y>
      <precision>4</precision>
      <show_units>true</show_units>
      <tooltip>Change lower non-recoverable threshold limit</tooltip>
      <increment>0.01</increment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>THRLOCRITVAL_Spinner</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoCrt-SP</pv_name>
      <x>380</x>
      <y>350</y>
      <precision>4</precision>
      <show_units>true</show_units>
      <tooltip>Change lower critical threshold limit</tooltip>
      <increment>0.01</increment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>THRLONONRECVAL_Spinner</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoNonRec-SP</pv_name>
      <x>380</x>
      <y>400</y>
      <precision>4</precision>
      <show_units>true</show_units>
      <tooltip>Change lower non-critical threshold limit</tooltip>
      <increment>0.01</increment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>THRLONONCRITVAL_Update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoNonCrt</pv_name>
      <x>250</x>
      <y>300</y>
      <width>120</width>
      <format>1</format>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Current lower non-recoverable threshold value</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>THRLOCRITVAL_Update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoCrt</pv_name>
      <x>250</x>
      <y>350</y>
      <width>120</width>
      <format>1</format>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Current lower critical threshold value</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>THRLONONRECVAL_Update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoNonRec</pv_name>
      <x>250</x>
      <y>400</y>
      <width>120</width>
      <format>1</format>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Current lower non-critical threshold value</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>THRUPNONRECVAL_Update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpNonRec</pv_name>
      <x>250</x>
      <y>80</y>
      <width>120</width>
      <format>1</format>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Current upper non-recoverable threshold value</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>THRUPCRITVAL_Update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpCrt</pv_name>
      <x>250</x>
      <y>130</y>
      <width>120</width>
      <format>1</format>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Current upper critical threshold value</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>THRUPNONCRITVAL_Update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpNonCrt</pv_name>
      <x>250</x>
      <y>180</y>
      <width>120</width>
      <format>1</format>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Current upper non-critical threshold value</tooltip>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>THRUPNONRECVAL_Spinner</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpNonRec-SP</pv_name>
      <x>380</x>
      <y>80</y>
      <precision>4</precision>
      <show_units>true</show_units>
      <tooltip>Change upper non-recoverable threshold limit</tooltip>
      <increment>0.01</increment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>THRUPCRITVAL_Spinner</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpCrt-SP</pv_name>
      <x>380</x>
      <y>130</y>
      <precision>4</precision>
      <show_units>true</show_units>
      <tooltip>Change upper critical threshold limit</tooltip>
      <increment>0.01</increment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>THRUPNONCRITVAL_Spinner</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpNonCrt-SP</pv_name>
      <x>380</x>
      <y>180</y>
      <precision>4</precision>
      <show_units>true</show_units>
      <tooltip>Change upper non-critical threshold limit</tooltip>
      <increment>0.01</increment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Commit_button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrCommit</pv_name>
      <text>Commit</text>
      <x>320</x>
      <y>450</y>
      <width>120</width>
      <tooltip>Load new thresholds values to module</tooltip>
    </widget>
    <widget type="led" version="2.0.0">
      <name>THRLONONCRITVAL_LED</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoNonCrtStat</pv_name>
      <x>20</x>
      <y>295</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>THRLOCRITVAL_LED</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoCrtStat</pv_name>
      <x>20</x>
      <y>345</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>THRLONONRECVAL_LED</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrLoNonRecStat</pv_name>
      <x>20</x>
      <y>395</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LowNonRec_label</name>
      <text>Lower Non-recoverable</text>
      <x>60</x>
      <y>400</y>
      <width>170</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LowCric_label</name>
      <text>Lower Critical</text>
      <x>60</x>
      <y>350</y>
      <width>170</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LowNonCric_label</name>
      <text>Lower Non-critical</text>
      <x>60</x>
      <y>300</y>
      <width>170</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>THRUPNONRECVAL_LED</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpNonRecStat</pv_name>
      <x>20</x>
      <y>75</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>THRUPCRITVAL_LED</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpCrtStat</pv_name>
      <x>20</x>
      <y>125</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>THRUPNONCRITVAL_LED</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrUpNonCrtStat</pv_name>
      <x>20</x>
      <y>175</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>UpNonRec_label</name>
      <text>Upper Non-recoverable</text>
      <x>60</x>
      <y>80</y>
      <width>170</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>UpCric_label</name>
      <text>Upper Critical</text>
      <x>60</x>
      <y>130</y>
      <width>170</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>UpNonCric_label</name>
      <text>Upper Non-critical</text>
      <x>60</x>
      <y>180</y>
      <width>170</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle</name>
      <x>320</x>
      <y>450</y>
      <width>120</width>
      <height>31</height>
      <line_color>
        <color name="ATTENTION" red="252" green="242" blue="17">
        </color>
      </line_color>
      <transparent>true</transparent>
      <rules>
        <rule name="Blinking" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0==1 &amp;&amp; pv1==1">
            <value>true</value>
          </exp>
          <exp bool_exp="pv0==1 &amp;&amp; pv1==0">
            <value>false</value>
          </exp>
          <exp bool_exp="pv0==0">
            <value>false</value>
          </exp>
          <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)ThrNotActual</pv_name>
          <pv_name>sim://ramp(0,1,0.5)</pv_name>
        </rule>
      </rules>
    </widget>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Close_button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[from org.csstudio.display.builder.runtime.script import ScriptUtil

ScriptUtil.closeDisplay(widget)]]></text>
        </script>
        <description>Execute Script</description>
      </action>
    </actions>
    <text>Close</text>
    <x>680</x>
    <y>760</y>
    <width>120</width>
    <height>40</height>
    <tooltip>Close window</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Sensor_group</name>
    <x>20</x>
    <y>590</y>
    <width>780</width>
    <height>150</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Sensor_rectangle</name>
      <width>780</width>
      <height>150</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Sensor_title</name>
      <class>HEADER3</class>
      <text>Sensor $(SENS)</text>
      <width>780</width>
      <height>40</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Slot_label</name>
      <text>Slot:</text>
      <x>140</x>
      <y>50</y>
      <width>50</width>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Name_label</name>
      <text>Name:</text>
      <x>310</x>
      <y>50</y>
      <width>50</width>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Type_label</name>
      <text>Type:</text>
      <x>410</x>
      <y>100</y>
      <width>50</width>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>FRU_update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)FruId</pv_name>
      <x>540</x>
      <y>50</y>
      <format>1</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>FRU ID of sensor</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>NAME_update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)FruName</pv_name>
      <x>370</x>
      <y>50</y>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Name of sensor</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Slot_update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Slot</pv_name>
      <x>200</x>
      <y>50</y>
      <format>1</format>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Slot of sensor</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Type_Sens_update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)Type</pv_name>
      <x>470</x>
      <y>100</y>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Type of sensor</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Val_Sens_update</name>
      <pv_name>$(P)$(MODULE)$(CRATE_NUM)$(IDX)Sen$(SENS)Value</pv_name>
      <x>300</x>
      <y>100</y>
      <precision>4</precision>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Value of sensor</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Value_label</name>
      <text>Value:</text>
      <x>240</x>
      <y>100</y>
      <width>50</width>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>FRU_label</name>
      <text>FRU:</text>
      <x>480</x>
      <y>50</y>
      <width>50</width>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Archvier_button</name>
    <actions>
      <action type="open_display">
        <file>../archiver/Archiver.bob</file>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Archiver</text>
    <x>550</x>
    <y>760</y>
    <width>110</width>
    <height>40</height>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
