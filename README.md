# opis-pbi-engineer

EPICS OPIs for PBI system to be used for (very) low-level development and testing in the control room(s).  

Content here is for OPI functionality intended for usage at a level lower than the displays used by PBI system leads. Typically the user would be an IOC developer like Hinko or Joao.  

Consequently as the primary users and the developers are usually the same people a small group of developers are allowed to add content directly to this repository without the overhead of approval workflow to accelerate deployment cycle. Secondary users like PBI system leads and operators can use these displays without restrictions but must note that these displays are optimised towards their primary use-case of developer usage.  

A repository administrator role (Thomas Fay and Joao) exists to maintain good housekeeping with regard to the file structure and content use-cases.  

## Conventions
The following directories exist for house-keeping reasons:  
10-Top  
99-Shared  
The '10-Top' directory is intended only for OPIs that can be directly opened by users. These OPIs are often 'laucher' type displays that instantiate template OPIs for a particular system.  
'99-Shared' directory is for objects that can be called/embedded from elsewhere. It generally does not make sense to open these objects directory in the OPI runtime. 
